Textastic-ReVIEW
================

## 1. これは何？


* [Textastic - Text, Code, and Markup Editor with Syntax Highlighting - FTP, SFTP, Dropbox - for iPad](http://www.textasticapp.com/)

用に作った、

* [kmuto/review](https://github.com/kmuto/review)

用の`.tmBundle`ファイルです。

## 2. 構文定義は[yanzm](https://github.com/yanzm)氏のSublime Text 2用のモノをお借りしました

このリポジトリに含まれている、

* `ReVIEW.tmBundle/Syntaxes/ReVIEW.tmLanguage`

は、Yuki Anzai氏が作られたSublime Text 2用の構文定義ファイル、

* <https://github.com/yanzm/ReVIEW>
  * →  <https://github.com/yanzm/ReVIEW/blob/master/review.tmLanguage>

をそのままコピーして使っております。

なお、上記ファイルを含め、Yuki Anzai氏のSublime Text 2用の設定ファイルは、

* <https://github.com/yanzm/ReVIEW/blob/master/LICENSE.txt>

にもあるように、Apache Licenseの元で提供されています。

## 3. 作者

Naoki OKAMURA (Nyarla) *nyarla[ at ]thotep.net*

## 4. ライセンス等

先にも書いた通り、このリポジトリに含まれる`ReVIEW.tmLanguage`ファイルは、
[yanzm](https://github.com/yanzm)氏がApache Licenseで提供している定義ファイルです。

なお、それ以外のファイル、

* `info.plist`

については、パブリックドメインとしておきます。
