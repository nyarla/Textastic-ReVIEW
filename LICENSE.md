COPYRIGHT
=========

`ReVIEW.tmBundle/Syntaxes/ReVIEW.tmLanguage`
--------------------------------------------

Copyright (C) Yuki Anzai all rights reserved.

This file is under the [Apache license](https://github.com/yanzm/ReVIEW/blob/master/LICENSE.txt)

This file is copied from:

* <https://github.com/yanzm/ReVIEW/blob/master/review.tmLanguage>

Other files
-----------

* `ReVIEW.tmBundle/info.plist`

This file is under the public domain.

This file is witten by Naoki OKAMURA (Nyarla) *nyarla[ at ]thotep.net*

